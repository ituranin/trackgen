# TrackGen

Dies ist ein Streckengenerator, der Formula Student Strecken mit geschlossenem Verlauf generieren kann. Im Notebook sind Beispiele für den Generierungsprozess. Entweder mit nur einer Strecke oder (auskommentiert) mit mehreren Strecken in mehreren parallelen Prozessen.

Die Strecken liegen dann im JSON-Format vor und können für CarlaBRC verwendet werden.

## Dependencies

`numpy, scipy, sympy, jsbeautifier`

## Funktionsweise

1. Generiere random Punkte
2. Erstelle eine konvexe Hülle um simpel eine geschlossene Basis-Strecke zu bekommen
3. Füge nach und nach Punkte zu den Hüllenpunkten hinzu und prüfe mögliche Kurvenwinkel
4. Generiere aus den Punkten einen Catmull-Rom-Spline
5. Leite die Streckengrenzen ab
6. Bestimme Kegel-Positionen auf den Grenzen in zufälligen Abständen
7. Erstelle Koordinaten für die Start/Ziel-Linie und einen Punkt für den Spawn des Fahrzeuges 6m davor (FSG Rules)

## Beispiel

* Rot - konvexe Hülle mit ursprünglichen Punkten
* Grün - Punkte aus Schritt 3
* Blau/Gelb - Streckengrenzen mit jeweiliger Kegel-Farbe
* Schwarz - Mittellinie (wichtig für den Autopiloten in der Sim)

![Strecke](beispiel_strecke_mit_hull.png)
